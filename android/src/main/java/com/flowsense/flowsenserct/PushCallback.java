package com.flowsense.flowsenserct;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactContext;
import com.flowsense.flowsensesdk.PushNotification.PushCallbacks;
import com.flowsense.flowsensesdk.PushNotification.FlowsenseNotification;

import java.util.Map;

public class PushCallback implements PushCallbacks {
    private ReactContext context;

    public PushCallback(ReactContext context){
        this.context = context;
    }

    @Override
    public void receivedNotification(FlowsenseNotification notification) {

        final Intent intent = new Intent(RNFlowsenseSDK.RECEIVED_PUSH);
        intent.putExtra("notification", notification);

        context.addLifecycleEventListener(new LifecycleEventListener() {
            @Override
            public void onHostResume() {
                context.sendBroadcast(intent);
                context.removeLifecycleEventListener(this);
            }

            @Override
            public void onHostPause() {

            }

            @Override
            public void onHostDestroy() {

            }
        });
    }

    @Override
    public void clickedNotification(FlowsenseNotification notification) {
        final Intent intent = new Intent(RNFlowsenseSDK.CLICKED_PUSH);
        intent.putExtra("notification", notification);

        context.addLifecycleEventListener(new LifecycleEventListener() {
            @Override
            public void onHostResume() {
                context.sendBroadcast(intent);
                context.removeLifecycleEventListener(this);
            }

            @Override
            public void onHostPause() {

            }

            @Override
            public void onHostDestroy() {

            }
        });
    }
}
