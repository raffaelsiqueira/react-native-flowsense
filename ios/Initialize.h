//
//  Initialize.h
//  RCTFlowsense
//
//  Created by Rafael on 05/12/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FlowsenseSDK/Service_fs.h>
#import "RNFlowsenseSDK.h"
#import "PushEvents.h"

NS_ASSUME_NONNULL_BEGIN

@interface Initialize : NSObject

@end

NS_ASSUME_NONNULL_END
