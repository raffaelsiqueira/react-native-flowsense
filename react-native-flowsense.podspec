require 'json'
package_json = JSON.parse(File.read('package.json'))

Pod::Spec.new do |s|
  s.name           = "react-native-flowsense"
  s.version        = package_json["version"]
  s.summary        = package_json["description"]
  s.homepage       = "https://www.flowsense.com.br"
  s.author         = { 'Flowsense' => 'tech@flowsense.com.br' }
  s.source         = { :http => 'file:' + __dir__ + '/ios' }
  s.platform     = :ios, "8.0"
  s.source_files   = 'ios/*.{h,m}'
  s.public_header_files = 'ios/**/*.h'
  s.dependency 'React',  '>= 0.13.0', '< 1.0.0'
  s.ios.vendored_frameworks = 'ios/frameworks/FlowsenseSDK.framework', 'ios/frameworks/AWSCore.framework', 'ios/frameworks/AWSKinesis.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework FlowsenseSDK -framework AWSCore -framework AWSKinesis' }
  s.ios.deployment_target = '8.0'
end
