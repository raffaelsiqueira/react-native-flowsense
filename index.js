'use strict';

import { NativeModules, NativeEventEmitter, Platform } from 'react-native';

const RCTFlowsenseSDK = NativeModules.FlowsenseSDK;
const myModuleEvt = new NativeEventEmitter(RCTFlowsenseSDK);

export default class FlowsenseSDK {
    static startFlowsenseService(token: string) {
        RCTFlowsenseSDK.startFlowsenseService(token);
    }

    static stopFlowsenseService() {
        RCTFlowsenseSDK.stopFlowsenseService();
    }

    static requestLocationAuthorizationStatus() {
        RCTFlowsenseSDK.requestLocationAuthorizationStatusAsync();
    }

    static requestWhenInUseAuthorizationWithCallback() {
        RCTFlowsenseSDK.requestWhenInUseAuthorizationWithCallback();
    }

    static requestAlwaysAuthorizationWithCallback() {
        RCTFlowsenseSDK.requestAlwaysAuthorizationWithCallback();
    }

    static requestWhenInUseAuthorization() {
        RCTFlowsenseSDK.requestWhenInUseAuthorization();
    }

    static requestAlwaysAuthorization() {
        RCTFlowsenseSDK.requestAlwaysAuthorization();
    }

    static startMonitoringLocation() {
        RCTFlowsenseSDK.startMonitoringLocation();
    }

    static updatePartnerUserID(identifier: string) {
        RCTFlowsenseSDK.updatePartnerUserID(identifier);
    }

    static setKeyValue(key: string, value: any) {
        switch (Object.prototype.toString.call(value)){
            case '[object Number]':
                RCTFlowsenseSDK.setKeyValueNumber(key, value);
                break;
            case '[object String]':
                RCTFlowsenseSDK.setKeyValueString(key, value);
                break;
            case '[object Boolean]':
                RCTFlowsenseSDK.setKeyValueBoolean(key, value);
                break;
            case '[object Date]':
                RCTFlowsenseSDK.setKeyValueDate('FSDate_' + key, new Date(value).getTime());
                break;
            default:
                break;
        }
    }

    static commitChanges() {
        RCTFlowsenseSDK.commitChanges();
    }

    static inAppEvent(eventName: string, eventMap: object) {
        RCTFlowsenseSDK.inAppEvent(eventName, eventMap);
    }

    static inAppEventLocalized(eventName: string, eventMap: object) {
        RCTFlowsenseSDK.inAppEventLocalized(eventName, eventMap);
    }

    static addListener(name: any, callback: Function) {
        myModuleEvt.addListener(name, callback);
    }

    static sendMessageToFlowsense(map) {
        RCTFlowsenseSDK.sendMessageToFlowsense(map);
    }

    static requestPushToken() {
        if (Platform.OS === 'ios') RCTFlowsenseSDK.requestPushToken();
    }

    static isAtHome(success, error) {
        if (Platform.OS === 'ios') RCTFlowsenseSDK.isAtHome((e, res) => {
            error(e);
            success(res);
        });
        else RCTFlowsenseSDK.isAtHome(success, error);
    }

    static isAtWork(success, error) {
        if (Platform.OS === 'ios') RCTFlowsenseSDK.isAtWork((e, res) => {
            error(e);
            success(res);
        });
        else RCTFlowsenseSDK.isAtWork(success, error);
    }

    static isAtPersonalPlace(success, error) {
        let home = new Promise((resolve, reject) => {
            FlowsenseSDK.isAtHome((res) => resolve(res), (e) => {if (e) reject(e)});
        });

        let work = new Promise((resolve, reject) => {
            FlowsenseSDK.isAtWork((res) => resolve(res), (e) => {if (e) reject(e)});
        });

        home.then((atHome) => {
            work.then((atWork) => {
                success(atHome, atWork);
            }).catch((e) => error(e));
        }).catch((e) => error(e));
    }

    static geolocalizedInAppEvent(eventName: string, latitude: number, longitude: number) {
        FlowsenseSDK.isAtPersonalPlace((atHome, atWork) => {
            let geoloc = (longitude !== null && longitude !== undefined && latitude !== null && latitude !== undefined) ? 
            {"type": "Point", "coordinates": [longitude, latitude]} : null;
            FlowsenseSDK.inAppEvent(eventName, {
                "geoloc": geoloc,
                "isAtHome": atHome,
                "isAtWork": atWork
            });
        }, (e) => console.log(e));
    }
}